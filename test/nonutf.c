/*
 * Test that deheader doesn't choke 
 * on the non-UTF-8 characters in the next line.
 *
 * <mapname> � � <key>
 */

#include <stdio.h>

int mail() {
  printf("ABC\n");
  return 0;
}
