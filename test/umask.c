/*
 * Items: umask(
 * Standardized-By: SuS
 * Detected-by: gcc-4.4.3 + Linux
 * Not-detected-by: BSD
 */

#include <sys/stat.h>
#include <sys/types.h>

int main(int arg, char **argv)
{
    umask(777);
}
