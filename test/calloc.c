/*
 * Items: calloc(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <stdlib.h>

// Required to shut up gcc
#define IGNORE(r)                                                              \
	do {                                                                   \
		if (r) {                                                       \
		}                                                              \
	} while (0)

int main(int arg, char **argv)
{
    IGNORE(calloc(0, 0));
}
