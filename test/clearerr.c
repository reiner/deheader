/*
 * Items: clearerr(
 * Standardized-By: SuS
 * Detected-by: gcc-4.4.3 + Linux
 */

#include <stdio.h>

int main(int arg, char **argv)
{
    (void)clearerr(stdin);
}
